package jokes.mindgame.com.telugujokes02

interface JokeInterfaces {
    fun loadJokeDetail()
    fun loadStartScreen()
    fun loadJokeHeader()
    fun loadTestModeScreen()
}