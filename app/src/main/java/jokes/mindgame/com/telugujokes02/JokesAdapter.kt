package jokes.mindgame.com.telugujokes02

import android.app.Activity
import android.content.Context
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.joke_view.view.*
import jokes.mindgame.com.telugujokes02.MainActivity.*

class JokesAdapter(val ctx:Context, val act:FragmentActivity?):RecyclerView.Adapter<JokesAdapter.ViewHolder>() {


    val admob = AdmobUtility(act)
    class ViewHolder(val view:View):RecyclerView.ViewHolder(view){

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(ctx).inflate(R.layout.joke_view,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return JokesDataset.jokes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.tvTitle.text =  "$position ." +JokesDataset.jokes.elementAt(position).title
        holder.view.tvLine1.text = JokesDataset.jokes.elementAt(position).line1

        holder.view.setOnClickListener(View.OnClickListener {
            JokesDataset.position = position
            if (act is JokeInterfaces){
               // act.loadJokeDetail()
                AdObject.admob.loadNextScreen { act.loadJokeDetail() }
            }

        })

    }


}