package jokes.mindgame.com.telugujokes02

 data class Joke(
 val title:String = "Joke",
 val line1:String = "",
 val line2:String = "",
 val line3:String = "",
 val line4:String = "",
 val line5:String = ""


 ){
     fun getFullJoke():String{
         val fulljoke = line1 +"\n"+line2+"\n"+line3+"\n"+line4
         return fulljoke
     }
 }
